package com.gsy.hmily.message;

import apache.rocketmq.v2.Message;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.gsy.hmily.dao.AccountInfoRes;
import com.gsy.hmily.popj.AccountInfoTxLog;
import com.gsy.hmily.service.AccountInfoService;
import com.gsy.hmily.service.RockMqAccountInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.producer.selector.SelectMessageQueueByHash;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Component
@RocketMQMessageListener(consumerGroup = "Group_Tow", topic = "OneAccountInfo",
        messageModel = MessageModel.CLUSTERING, consumeMode = ConsumeMode.CONCURRENTLY)
public class ConsumerListener  implements RocketMQListener<AccountInfoTxLog>{

    @Autowired
    public AccountInfoService accountInfoService;

    @Autowired
    public RockMqAccountInfoService rockMqAccountInfoService;

    @Autowired
    public AccountInfoRes accountInfoRes;

    /**
     * 直接在监听方法中处理
     * @param accountInfoTxLog
     */
    //@Override
    //@Transactional
    //public void onMessage(AccountInfoTxLog accountInfoTxLog) {
    //    log.info("onMessage tx ==> 直接在监听方法中处理");
    //    System.out.println("onMessage ==> " + accountInfoTxLog.toString());
    //    String tx = accountInfoTxLog.getTx();
    //    if (accountInfoRes.isTxExists(tx) == 0) {
    //        System.out.println("onMessage tx ==> " + tx);
    //        accountInfoService.sq(accountInfoTxLog.getAccountInfo().getCardNumber(),
    //                accountInfoTxLog.getAccountInfo().getBalance());
    //        int a = 1/0;
    //        accountInfoRes.addTx(tx);
    //    }
    //}

    /**
     * 发送事务消息，在本地事务消息中处理，异常的时候发送回滚信息通知调用方。
     * @param accountInfoTxLog
     */
    @Override
    @Transactional
    public void onMessage(AccountInfoTxLog accountInfoTxLog) {
            log.info("onMessage tx ==> 发送事务消息，在本地事务消息中处理，异常的时候发送回滚信息通知调用方");
            log.info("executeLocalTransaction b2 ==> " + accountInfoTxLog);
            rockMqAccountInfoService.sendTransactionMessage(accountInfoTxLog);

    }

    //@Override
    //public void onMessage(Object o) {
    //    JSON parse = JSONUtil.parse(o);
    //    AccountInfoTxLog accountInfoTxLog = parse.toBean(AccountInfoTxLog.class);
    //    System.out.println("onMessage ==> " + accountInfoTxLog.toString());
    //}

}
