package com.gsy.hmily.message;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.gsy.hmily.popj.AccountInfoTxLog;
import org.apache.rocketmq.client.consumer.DefaultLitePullConsumer;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.*;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.selector.SelectMessageQueueByHash;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class customConsumerListener {

    @Value("${rocketmq.name-server:#{null}}")
    public String nameServer;

    @Value("${rocketmq.consumer.group:#{null}}")
    public String group;

    @Value("${topic.a:#{null}}")
    public String topic;

    //@PostConstruct
    public void customConsumer() throws MQClientException {
        System.out.println("topic ==> " + topic);
        System.out.println("nameServer ==> " + nameServer);
        System.out.println("group ==> " + group);
        DefaultMQPushConsumer defaultMQPushConsumer = new DefaultMQPushConsumer(group);
        defaultMQPushConsumer.setNamesrvAddr(nameServer);
        defaultMQPushConsumer.subscribe(topic,"*");
        defaultMQPushConsumer.setMessageModel(MessageModel.BROADCASTING);
        defaultMQPushConsumer.setMessageListener(new MessageListenerOrderly() {
            @Override
            public ConsumeOrderlyStatus consumeMessage(List<MessageExt> list, ConsumeOrderlyContext consumeOrderlyContext) {
                for (MessageExt messageExt : list) {
                    String o = new String(messageExt.getBody());
                    JSON parse = JSONUtil.parse(o);
                    AccountInfoTxLog accountInfoTxLog = parse.toBean(AccountInfoTxLog.class);
                    System.out.println("consumeMessage ==> " + accountInfoTxLog.toString());
                }
                System.out.println("ConsumeOrderlyStatus.SUCCESS");
                return ConsumeOrderlyStatus.SUCCESS;
            }
        });
        defaultMQPushConsumer.start();
    }
}
