CREATE TABLE `local_rockmq_log`
(
    `tx_no`       varchar(64) NOT NULL COMMENT '事务id',
    `create_time` datetime DEFAULT NULL,
    PRIMARY KEY (`tx_no`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE `local_rockmq_rollback_log`
(
    `tx_no`       varchar(64) NOT NULL COMMENT '事务id',
    `create_time` datetime DEFAULT NULL,
    PRIMARY KEY (`tx_no`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;