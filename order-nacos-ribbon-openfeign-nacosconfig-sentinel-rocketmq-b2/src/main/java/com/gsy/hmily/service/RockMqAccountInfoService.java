package com.gsy.hmily.service;

import cn.hutool.json.JSONUtil;
import com.gsy.hmily.dao.AccountInfoRes;
import com.gsy.hmily.popj.AccountInfo;
import com.gsy.hmily.popj.AccountInfoTxLog;
import lombok.extern.log4j.Log4j2;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
@RocketMQTransactionListener()
public class RockMqAccountInfoService implements RocketMQLocalTransactionListener {



    @Autowired
    public AccountInfoRes accountInfoRes;

    @Autowired
    public RocketMQTemplate rocketMQTemplate;

    @Autowired
    public RockMqAccountInfoService rockMqAccountInfoService;



    @Transactional
    public AccountInfo sq(int id, int s) throws ParseException {
        log.info("sq  ==>");
        Optional<AccountInfo> byId = accountInfoRes.findById(id);
        if (byId.isPresent()){
            AccountInfo accountInfo = byId.get();
            accountInfo.setBalance(accountInfo.getBalance() + s);
            AccountInfo save = accountInfoRes.save(accountInfo);
            log.info("sq: " + save.toString());

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d1 = new Date();
            Date d2 = df.parse("2023-03-15 10:31:00");
            long diff = d2.getTime() - d1.getTime();
            log.info("time: " + diff);
            if (diff > 0) {
                int c = 1 / 0;
            }
            log.info("time: success");
            return save;
        }
        return null;
    }
    @Transactional
    public AccountInfo sendTransactionMessage(AccountInfoTxLog accountInfoTxLog){
        log.info("sendTransactionMessage  ==> " + accountInfoTxLog.getTx());
        AccountInfoTxLog accountInfoTxLog1 = new AccountInfoTxLog();
        accountInfoTxLog1.setTx(accountInfoTxLog.getTx());
        accountInfoTxLog1.setAccountInfo(accountInfoTxLog.getAccountInfo());
        accountInfoTxLog1.setStatus("s");
        AccountInfoTxLog accountInfoTxLog2 = new AccountInfoTxLog();
        accountInfoTxLog2.setTx(accountInfoTxLog.getTx());
        accountInfoTxLog2.setAccountInfo(accountInfoTxLog.getAccountInfo());
        accountInfoTxLog2.setStatus("e");
        Message<AccountInfoTxLog> build1 = MessageBuilder.withPayload(accountInfoTxLog1).build();
        Message<AccountInfoTxLog> build2 = MessageBuilder.withPayload(accountInfoTxLog2).build();
        log.info("sendTransactionMessage  build1==> " + build1.getPayload());
        log.info("sendTransactionMessage  build2==> " + build2.getPayload());
        TransactionSendResult sendResult1 = rocketMQTemplate.sendMessageInTransaction("OneCallBack", build1, accountInfoTxLog.getTx());
        TransactionSendResult sendResult2 = rocketMQTemplate.sendMessageInTransaction("OneCallBack", build2, accountInfoTxLog.getTx());
        log.warn("sendTranscationMessage bank2 ==> " + sendResult1.getSendStatus().toString());
        log.warn("sendTranscationMessage bank2 ==> " + sendResult2.toString());
        return accountInfoRes.findById(accountInfoTxLog.getAccountInfo().getCardNumber()).get();
    }

    @Transactional
    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(Message message, Object o) {
        log.info("executeLocalTransaction 1 ==> " + (String)o);
        AccountInfoTxLog accountInfoTxLog = JSONUtil.parseObj(message.getPayload()).toBean(AccountInfoTxLog.class);
        log.info("executeLocalTransaction 2 ==> " + accountInfoTxLog);
        try {
            if (accountInfoRes.isTxExists((String) o) == 0) {
                rockMqAccountInfoService.sq(accountInfoTxLog.getAccountInfo().getCardNumber(),
                        accountInfoTxLog.getAccountInfo().getBalance());
                log.info("executeLocalTransaction 3 ==> " + accountInfoTxLog);
                accountInfoRes.addTx((String) o);
            }
            log.info("executeLocalTransaction 4 ==> " + accountInfoTxLog);
            if (accountInfoTxLog.getStatus().equals("s")){
                log.info("executeLocalTransaction 5 ==> " + accountInfoTxLog);
                return RocketMQLocalTransactionState.COMMIT;
            }
            log.info("executeLocalTransaction 6 ==> " + accountInfoTxLog);
            return RocketMQLocalTransactionState.ROLLBACK;
        } catch (Exception e) {
            log.warn("sendTranscationMessage bank2 ==> RocketMQLocalTransactionState.COMMIT");
            if (accountInfoTxLog.getStatus().equals("e")){
                return RocketMQLocalTransactionState.COMMIT;
            }
            return RocketMQLocalTransactionState.ROLLBACK;
        }
    }

    @Transactional
    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message message) {
        log.info("checkLocalTransaction  ==> ");
        AccountInfoTxLog accountInfoTxLog = JSONUtil.parseObj(message.getPayload()).toBean(AccountInfoTxLog.class);
        if (accountInfoRes.isTxExists(accountInfoTxLog.getTx()) > 0) {
            if (accountInfoTxLog.getStatus().equals("s")){
                return RocketMQLocalTransactionState.COMMIT;
            }
            return RocketMQLocalTransactionState.ROLLBACK;
        }
        log.warn("sendTranscationMessage bank2 ==> RocketMQLocalTransactionState.UNKNOWN");
        return RocketMQLocalTransactionState.UNKNOWN;
    }
}
