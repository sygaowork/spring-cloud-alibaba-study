package com.gsy.hmily.service;

import com.gsy.hmily.dao.AccountInfoRes;
import com.gsy.hmily.popj.AccountInfo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class AccountInfoService {


    @Autowired
    public AccountInfoRes accountInfoRes;

    public List<AccountInfo> getAll(){
        List<AccountInfo> all = accountInfoRes.findAll();
        return all;
    }

    @Transactional
    public AccountInfo sq(int id, int s){
        Optional<AccountInfo> byId = accountInfoRes.findById(id);
        if (byId.isPresent()){
            AccountInfo accountInfo = byId.get();
            accountInfo.setBalance(accountInfo.getBalance() + s);
            AccountInfo save = accountInfoRes.save(accountInfo);
            log.info("sq: " + save.toString());
            return save;
        }
        return new AccountInfo();
    }

}
