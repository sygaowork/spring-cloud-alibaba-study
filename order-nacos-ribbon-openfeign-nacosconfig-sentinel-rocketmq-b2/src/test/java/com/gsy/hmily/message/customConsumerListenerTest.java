package com.gsy.hmily.message;

import org.apache.rocketmq.client.exception.MQClientException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class customConsumerListenerTest {

    @Autowired
    customConsumerListener customConsumerListener;

    @Test
    void customConsumer() throws MQClientException {
        customConsumerListener.customConsumer();
    }
}