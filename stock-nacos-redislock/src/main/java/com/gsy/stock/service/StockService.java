package com.gsy.stock.service;

import com.gsy.stock.dao.StockRes;
import com.gsy.stock.popj.Stock;
import com.gsy.stock.redislock.RedisLockOne;
import com.gsy.stock.redislock.RedisLockThree;
import com.gsy.stock.redislock.RedisLockTow;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

@Service
public class StockService {

    public ReentrantLock lock = new ReentrantLock();

    @Autowired
    public RedisTemplate<Object, Object> redisTemplate;

    @Autowired
    public StockRes stockRes;

    @Autowired
    public RedissonClient redissonClient;

    public String decr(String name) throws InterruptedException {

        RLock rLock = redissonClient.getLock("name");
        boolean tryLock = rLock.tryLock(90,  90, TimeUnit.SECONDS);
        try {
            if (tryLock) {
                Stock topByName = stockRes.findByName(name);
                topByName.setProductId(topByName.getProductId() - 1);
                Stock project = stockRes.saveAndFlush(topByName);
                System.out.println("商品减少" + name + "线程id： " + Thread.currentThread().getId());
                return "商品减少 " + name;
            }
        }finally {
            rLock.unlock();
        }
          return "decr 异常";
    }


//    public String decr(String name) throws InterruptedException {
//        RedisLockThree redisLockThree = new RedisLockThree();
//        redisLockThree.setRedisTemplate(redisTemplate);
//        redisLockThree.setLockName(name);
//        while (!redisLockThree.tryLock(1000)); {
//            int i = 5;
//        }
//        Stock topByName = stockRes.findByName(name);
//        topByName.setProductId(topByName.getProductId() - 1);
//        Stock project = stockRes.saveAndFlush(topByName);
//        redisLockThree.unLock();
//        System.out.println("商品减少" + name + "线程id： " +  Thread.currentThread().getId());
//        return "商品减少" + name;
//    }

}
