package com.gsy.stock.dao;

import com.gsy.stock.popj.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRes extends JpaRepository<Stock, Integer> {
    Stock findByName(String name);
}
