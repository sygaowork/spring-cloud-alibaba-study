package com.gsy.stock.redislock.redisson;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.TimeUnit;

public class RedissonOne {

    @Autowired
    public RedissonClient redissonClient;

    public void  exp () throws InterruptedException {
        RLock lock = redissonClient.getLock("key");
        // 1 获取锁的最大等待时间， 2 自动释放锁的时间 3 时间单位
        boolean getLock = lock.tryLock(5, 5, TimeUnit.SECONDS);
        try{
            if(getLock) {

            }
        }finally {
            lock.unlock();
        }
    }

}
