package com.gsy.stock.redislock;

import com.alibaba.nacos.common.utils.UuidUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.time.Duration;

/**
 * 增加了锁标识字段
 * 防止 一个阻塞后，锁超时后，其他的线程拿到锁但是 之前的线程执行完执行 释放了其他线程的锁
 */
@Slf4j
@Data
public class RedisLockTow {

    public RedisTemplate<Object, Object> redisTemplate;

    public String lockName = "Red is lock";

    public String uuid = UuidUtils.generateUuid();

    public boolean tryLock(long exTime){
        BoundValueOperations<Object, Object> ops = redisTemplate.boundValueOps(lockName);
        Boolean res = ops.setIfAbsent(Thread.currentThread().getId() + uuid, Duration.ofSeconds(exTime));
        return Boolean.TRUE.equals(res);
    }

    public boolean unLock(){
        Boolean res = false;
        BoundValueOperations<Object, Object> ops = redisTemplate.boundValueOps(lockName);
        if(ops.get().equals(Thread.currentThread().getId() + uuid)) {
            res = redisTemplate.delete(lockName);
            log.info("正常释放锁" + lockName + " : " + Thread.currentThread().getId() + uuid);
            return Boolean.TRUE.equals(res);
        }
        log.warn("没有 正常释放锁");
        return false;
    }
}
