package com.gsy.stock.redislock;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Data
public class RedisLockOne {

    public RedisTemplate<Object, Object> redisTemplate;

    public String lockName = "Red is lock";

    public boolean tryLock(long exTime){
        BoundValueOperations<Object, Object> ops = redisTemplate.boundValueOps(lockName);
        Boolean res = ops.setIfAbsent(Thread.currentThread().getId(), Duration.ofSeconds(exTime));
        return Boolean.TRUE.equals(res);
    }

    public boolean unLock(){
        Boolean res = redisTemplate.delete(lockName);
        return Boolean.TRUE.equals(res);
    }
}
