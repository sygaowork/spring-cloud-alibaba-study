package com.gsy.stock.redislock;

import com.alibaba.nacos.common.utils.UuidUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import java.time.Duration;
import java.util.Collections;

/**
 * 增加了锁标识字段, 并且用Lua脚本进行unlock
 * 防止 一个在 判断完释放锁的时候 阻塞后，锁超时后，其他的线程拿到锁但是 之前的线程执行完执行 释放了其他线程的锁
 */
@Slf4j
@Data
public class RedisLockThree {

    public RedisTemplate<Object, Object> redisTemplate;

    public String lockName = "Red is lock";

    public String uuid = UuidUtils.generateUuid();

    public static final DefaultRedisScript<Long> UN_LOCK_LUA_SCRIPT;
    static {
        UN_LOCK_LUA_SCRIPT = new DefaultRedisScript<Long>();
        UN_LOCK_LUA_SCRIPT.setLocation(new ClassPathResource("unlock.lua"));
        UN_LOCK_LUA_SCRIPT.setResultType(Long.class);
    }

    public boolean tryLock(long exTime){
        BoundValueOperations<Object, Object> ops = redisTemplate.boundValueOps(lockName);
        Boolean res = ops.setIfAbsent(Thread.currentThread().getId() + uuid, Duration.ofSeconds(exTime));
        return Boolean.TRUE.equals(res);
    }

    public boolean unLock(){
        Long execute = redisTemplate.execute(UN_LOCK_LUA_SCRIPT,
                Collections.singletonList(lockName),
                Thread.currentThread().getId() + uuid);
        return execute == 1;
    }
}
