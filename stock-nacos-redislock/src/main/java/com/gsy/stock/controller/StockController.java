package com.gsy.stock.controller;

import com.gsy.stock.popj.Stock;
import com.gsy.stock.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/stock")
public class StockController {

    @Value("${server.port}")
    String port;

    @Autowired
    public StockService stockService;

    @RequestMapping("/reduck")
    public String reduck(){
        System.out.println("扣减库存");
        return "hello world" + port;
    }

    @RequestMapping("/dric/{name}")
    public String dric(@PathVariable("name") String name) throws InterruptedException {
        stockService.decr(name);
        System.out.println("扣减库存");
        return "hello world";
    }
}
