package com.gsy.stock.popj;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


@Table
@Entity
@Data
public class MysqlLockLog implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String uuId;
    @Column
    private String version;
}
