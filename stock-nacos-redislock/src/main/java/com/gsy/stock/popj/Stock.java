package com.gsy.stock.popj;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


@Table
@Entity
@Data
public class Stock implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;
    @Column
    private Integer productId;
}
