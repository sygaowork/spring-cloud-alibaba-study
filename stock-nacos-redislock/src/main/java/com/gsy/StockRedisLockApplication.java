package com.gsy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockRedisLockApplication {
    public static void main(String[] args) {
        SpringApplication.run(StockRedisLockApplication.class,args);
    }
}
