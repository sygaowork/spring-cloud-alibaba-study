package com.gsy.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/add")
    public String add() throws InterruptedException {
        System.out.println("下单成功 食物");
        Thread thread = null;
        for (int i = 0; i < 1000; i++) {
             thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    restTemplate.getForObject("http://stock-service/stock/dric/食物", String.class);
                }
            });
            thread.start();
        }
        System.out.println("下单成功 药品");
        Thread thread2 = null;
        for (int i = 0; i < 1000; i++) {
            thread2 = new Thread(new Runnable() {
                @Override
                public void run() {
                    restTemplate.getForObject("http://stock-service/stock/dric/药品", String.class);
                }
            });
            thread2.start();
        }
        thread.join();
        thread2.join();
        return "hello wrold order2-nacos";
    }
}
