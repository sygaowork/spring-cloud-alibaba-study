package com.gsy.hmily.service;

import com.esotericsoftware.minlog.Log;
import com.gsy.hmily.dao.AccountInfoRes;
import com.gsy.hmily.dao.TccRes;
import com.gsy.hmily.dao.fegin.bankTowRes;
import com.gsy.hmily.popj.AccountInfo;
import org.dromara.hmily.annotation.Hmily;
import org.dromara.hmily.core.concurrent.threadlocal.HmilyTransactionContextLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Service
public class HmilyAccountInfoService {


    @Autowired
    public AccountInfoRes accountInfoRes;

    @Autowired
    public TccRes TccRes;

    @Autowired
    public bankTowRes bankTowRes;


    @Hmily(confirmMethod = "confirmZq", cancelMethod = "cancelZq")
    @Transactional
    public AccountInfo tryZq(int zid, int sid, int b){
        //获取全局事务idHmilyTransactionContextLocal.getInstance.get().getTransIdl
        //String transId = new HmilyTransactionContext().getTransId().toString();
        // 2.0.6
        String transId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        Log.info("bank1 tryZq  ====> " + transId);
        //幂等性
        if (TccRes.isExistTry(transId) > 0) return null;
        //防止悬挂
        if (TccRes.isExistCancel(transId) > 0 ) return null;
        //本地事务
        //try {
            Optional<AccountInfo> byId = accountInfoRes.findById(zid);
            if (byId.isPresent()){
                AccountInfo accountInfo = byId.get();
                accountInfo.setBalance(accountInfo.getBalance()-b);
                AccountInfo save = accountInfoRes.save(accountInfo);
                Log.info("zq ====> " + save.toString());
                //更新幂等
                TccRes.addTry(transId);
                //执行分布式事务方法。同步
                AccountInfo accountInfo1 = bankTowRes.HmilyshouQian(sid, b);
                //执行分布式事务方法。异步
                //send(sid, b);
                //if (accountInfo1 == null) {
                //    throw new RuntimeException("远程调用返回null");
                //}
                //int a = 1/0;
                return save;
            }
        //}catch (Exception e) { }
        return null;
    };
    @Transactional
    public AccountInfo confirmZq(int zid, int sid, int b){
        Log.info("bank1 ====> confirmZq");
        return new AccountInfo();
    };

    @Async
    public void send(int sid, int b){
        bankTowRes.HmilyshouQian(sid, b);
    }


    @Transactional
    public AccountInfo cancelZq(int zid, int sid, int b){
        String transId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        //String transId = new HmilyTransactionContext().getTransId().toString();
        Log.info("bank1 cancelZq  ====> " + transId);
        //幂等性
        if (TccRes.isExistCancel(transId) > 0) return null;
        //空回滚
        if (TccRes.isExistTry(transId) <= 0) return null;
        //本地事务
        Optional<AccountInfo> byId = accountInfoRes.findById(zid);
        if (byId.isPresent()){
            AccountInfo accountInfo = byId.get();
            accountInfo.setBalance(accountInfo.getBalance()+b);
            AccountInfo save = accountInfoRes.save(accountInfo);
            Log.info("zq: " + save.toString());
            //完成幂等
            TccRes.addCancel(transId);
            return save;
        }
        return null;
    };

}
