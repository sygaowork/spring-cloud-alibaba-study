package com.gsy.hmily.service;

import com.esotericsoftware.minlog.Log;
import com.gsy.hmily.dao.AccountInfoRes;
import com.gsy.hmily.dao.fegin.bankTowRes;
import com.gsy.hmily.popj.AccountInfo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class AccountInfoService {


    @Autowired
    public AccountInfoRes accountInfoRes;

    @Autowired
    public bankTowRes bankTowRes;

    public List<AccountInfo> getAll(){
        List<AccountInfo> all = accountInfoRes.findAll();
        List<AccountInfo> all1 = bankTowRes.getAll();
        all.addAll(all1);
        return all;
    }

    @Transactional
    public AccountInfo zq(int zid, int sid, int b){
        Optional<AccountInfo> byId = accountInfoRes.findById(zid);
        if (byId.isPresent()){
            AccountInfo accountInfo = byId.get();
            accountInfo.setBalance(accountInfo.getBalance()-b);
            AccountInfo save = accountInfoRes.save(accountInfo);
            AccountInfo accountInfo1 = bankTowRes.shouQian(sid, b);
            Log.info("zq: " + save.toString());
            Log.info("zq: " + accountInfo1.toString());
            int a = 1/0;
            return save;
        }

        return null;
    }

}
