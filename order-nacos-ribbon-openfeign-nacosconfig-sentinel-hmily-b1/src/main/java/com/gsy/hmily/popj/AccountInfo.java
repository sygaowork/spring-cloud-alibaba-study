package com.gsy.hmily.popj;


import lombok.Data;
import net.bytebuddy.dynamic.loading.ClassReloadingStrategy;
import rx.BackpressureOverflow;

import javax.persistence.*;

@Data
@Entity
@Table
public class AccountInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String name;
    public Integer cardNumber;
    public String password;
    public Integer balance;
}
