package com.gsy.hmily.contorller;

import com.gsy.hmily.popj.AccountInfo;
import com.gsy.hmily.service.AccountInfoService;
import com.gsy.hmily.service.HmilyAccountInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/bank1")
public class AccountInfoController {

    @Autowired
    public AccountInfoService accountInfoService;

    @Autowired
    public HmilyAccountInfoService hmilyAccountInfoService;

    @RequestMapping("/getAll")
    public List<AccountInfo> getAll() {
        return accountInfoService.getAll();
    }

    @RequestMapping("/zhuanQian")
    public AccountInfo zhuanQian() {
        return  accountInfoService.zq(2, 2, 10000);
    }

    @RequestMapping("/HmilyZhuanQian")
    public AccountInfo HmilyZhuanQian() {
        return hmilyAccountInfoService.tryZq(2, 2, 10000);
    }

}
