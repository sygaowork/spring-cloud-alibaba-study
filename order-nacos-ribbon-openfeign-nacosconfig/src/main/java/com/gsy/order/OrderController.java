package com.gsy.order;

import com.gsy.stockService.StockService;
import com.gsy.productService.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {

    //@Autowired   用了openfeign 所以注释
    //RestTemplate restTemplate;

    @Autowired
    StockService order;
    @Autowired
    ProductService product;

    @RequestMapping("/add")
    public String add(){
        System.out.println("下单成功");
        //String message = restTemplate.getForObject("http://stock-service/stock/reduck", String.class);
        String message = order.reduck();
        System.out.println("查询产品");
        String s = product.get(2);
        return "hello open feign" + "" + message + " " + s;
    }
    @RequestMapping("/readTimeout/add")
    public String readTimeoutAdd(){
        System.out.println("下单成功");
        //String message = restTemplate.getForObject("http://stock-service/stock/reduck", String.class);
        String message = order.reduck();
        System.out.println("查询产品");
        String s = product.getReadTimeout(3);
        return "hello open feign" + "" + message + " " + s;
    }
}
