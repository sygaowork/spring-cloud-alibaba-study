package com.gsy.configs;

import com.gsy.interseptor.custom.FeginCustomInterseptor;
import feign.Logger;
import feign.Request;
import org.springframework.context.annotation.Bean;

//配置类的方式：
// 使用Configuraion直接，会对所哟䣌 feignClient生效； 数据全局配置，
// 若是用局部配置的话 就不要使用 Configuration， 在feignClient 的参数中指定配置了类。

//@Configuration
public class MyOpenFeginConfig {
    @Bean
    public Logger.Level MyfeignClentLoggers() {
        return Logger.Level.FULL;
    }
    //配置类的方式配置 连接超市 和 响应超时 时间 Request.Options 第一个为连接超时，第二个响应超时。也可以采取配置文件
    @Bean
    public Request.Options options (){
        return new Request.Options(5000, 10000);
    }
    //配置自定义拦截器。 也可以采取配置文件。
    @Bean
    public FeginCustomInterseptor feginCustomInterseptor (){
        return new FeginCustomInterseptor();
    }
}
