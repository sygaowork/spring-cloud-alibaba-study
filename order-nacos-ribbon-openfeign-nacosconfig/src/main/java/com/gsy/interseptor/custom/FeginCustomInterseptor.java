package com.gsy.interseptor.custom;

import feign.RequestInterceptor;
import feign.RequestTemplate;

public class FeginCustomInterseptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        System.out.println("feign 自定义拦截器");
    }
}
