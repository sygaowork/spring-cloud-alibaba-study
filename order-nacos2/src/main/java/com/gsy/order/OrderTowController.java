package com.gsy.order;

import org.apache.http.HttpEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/orderTow")
public class OrderTowController {

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/addTow")
    public String add() throws InterruptedException {
        System.out.println("下单成功");
        Thread thread = null;
        for (int i = 0; i < 10000; i++) {
             thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    restTemplate.getForObject("http://stock-service/stock/dric", String.class);
                }
            });
            thread.start();
        }
        thread.join();
        return "hello wrold order-nacos";
    }
}
