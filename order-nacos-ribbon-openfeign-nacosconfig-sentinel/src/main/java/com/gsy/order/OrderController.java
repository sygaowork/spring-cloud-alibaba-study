package com.gsy.order;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.gsy.stockService.StockService;
import com.gsy.productService.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {

    //@Autowired   用了openfeign 所以注释
    //RestTemplate restTemplate;

    @Autowired
    StockService order;

    @Qualifier("com.gsy.productService.ProductService")
    @Autowired
    ProductService product;
    // 流控规则同归页面配置的qps， 要看代码配置方式 看sentinelDome中。
    @RequestMapping("/addQpsFlows")
    @SentinelResource(value = "addaddQps", blockHandler = "addQpsBlock")
    public String addQpsFlow(){
        System.out.println("下单成功");
        //String message = restTemplate.getForObject("http://stock-service/stock/reduck", String.class);
        String message = order.reduck();
        System.out.println("查询产品");
        String s = product.get(2);
        return "hello open feign" + "" + message + " " + s;
    }
    public String addQpsBlock(BlockException e){
        System.out.println("QPS限流");
        return "QPS限流";
    }
    // 流控规则同归页面配置的线程数， 要看代码配置方式 看sentinelDome中。
    @RequestMapping("/addThreadFlows")
    @SentinelResource(value = "addThread" , blockHandler = "addTheadBlock")
    public String addThreadFlow() throws InterruptedException {
        Thread.sleep(5000);
        System.out.println("下单成功");
        //String message = restTemplate.getForObject("http://stock-service/stock/reduck", String.class);
        String message = order.reduck();
        System.out.println("查询产品");
        String s = product.get(2);
        return "hello open feign" + "" + message + " " + s;
    }

    public String addTheadBlock(BlockException e) {
        System.out.println("线程数限流");
        return "线程数限流";
    }
    // 为了测试feign-client-config-服务名称- 连接超时，响应超时，拦截器，日志级别等配置。
    @RequestMapping("/readTimeout/add")
    public String readTimeoutAdd(){
        System.out.println("下单成功");
        //String message = restTemplate.getForObject("http://stock-service/stock/reduck", String.class);
        String message = order.reduck();
        System.out.println("查询产品");
        String s = product.getReadTimeout(3);
        return "hello open feign" + " " + message + " " + s;
    }

    //为了测试openfeign和sentinel整合后（feign-sentinel-enable： true） 当服务提供报错的时候，会调用openfeign接口化服务实现类中的方法。
    @RequestMapping("/openFeign/add")
    public String openFeignAdd(){
        System.out.println("openFeign 下单成功");
        //String message = restTemplate.getForObject("http://stock-service/stock/reduck", String.class);
        String message = order.reduck();
        System.out.println(" openFeign 查询产品");
        String s = product.getException(5);
        return "hello open feign sentinel" + " " + message + " " + s;
    }
}
