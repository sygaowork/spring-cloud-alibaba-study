package com.gsy.productService;

import com.gsy.prodectserviceimp.ProductServiceimp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "product-service", path = "/product", fallback = ProductServiceimp.class)
public interface ProductService {
    @RequestMapping("/{id}")
    public String get(@PathVariable("id") Integer id);

    @RequestMapping("/getReadTimeout/{id}")
    public String getReadTimeout(@PathVariable("id") Integer id);

    @RequestMapping("/getException/{id}")
    public String getException(@PathVariable("id") Integer id);
}
