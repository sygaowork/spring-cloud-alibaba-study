package com.gsy.prodectserviceimp;

import com.gsy.productService.ProductService;
import org.springframework.stereotype.Component;

/**
 * openFeign接口话 服务的实现鳄梨， 加上配置 feign-sentinel-enable： true 。 整合sentinel的降级服务。
 * 当接口服务报错时候会调用实现类中的降级方法
 */
@Component
public class ProductServiceimp implements ProductService {
    @Override
    public String get(Integer id) {
        return "open Feign sentinel";
    }

    @Override
    public String getReadTimeout(Integer id) {
        return "open Feign sentinel";
    }

    @Override
    public String getException(Integer id) {
        return "open Feign sentinel !!!";
    }
}
