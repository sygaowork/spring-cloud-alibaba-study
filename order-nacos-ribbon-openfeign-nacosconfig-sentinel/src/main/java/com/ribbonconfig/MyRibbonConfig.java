package com.ribbonconfig;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
// 注意不能在扫描包的 ComponentScan 的basePackage， 如果在的换就没法指定每一个服务提供者的负载均衡策略的效果。
@Configuration
public class MyRibbonConfig {
    @Bean
    public IRule iRule(){
        return new RandomRule();
    }
}
