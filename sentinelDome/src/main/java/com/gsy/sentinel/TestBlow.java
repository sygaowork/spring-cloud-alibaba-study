package com.gsy.sentinel;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.gsy.sentinelRule.BlowRules;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestBlow {

    @RequestMapping("/blow/{id}")
    @SentinelResource(value = "testBlow", blockHandler = "blockHandlerTestBlow", blockHandlerClass = BlowRules.class)
    public String testBlow(@PathVariable(value = "id") String id) {
        throw new RuntimeException("测试异常触发熔断规则");
        //return id + " " + "testBlow";
    }
}
