package com.gsy.sentinel;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.gsy.sentinelRule.CurrenLimitingRules;
import com.gsy.sentinelRule.ExecptionRule;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class testException {
    // fallback 为异常处理的指定方法， 当限流也存在的时候， 限流规则会优先。
    @RequestMapping("/testException/{id}")
    @SentinelResource(value = "testException", blockHandler = "blockHandlerCurreniLmitingFunc" ,blockHandlerClass = CurrenLimitingRules.class,
            fallback = "blockHandlerTestExceptionFunc" ,fallbackClass = ExecptionRule.class)
    public String testException(@PathVariable(value = "id") String id) {
        System.out.println(id + " " + "testException");
        double res = 1/0;
        return id + " " + "CurrenLimiting";
    }
}
