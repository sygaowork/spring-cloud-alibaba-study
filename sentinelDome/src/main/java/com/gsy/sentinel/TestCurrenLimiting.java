package com.gsy.sentinel;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.gsy.sentinelRule.CurrenLimitingRules;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestCurrenLimiting {
/* SentinelResource() 需要依赖sentinel-annotation-aspectj  , 在启动类配置bean 规则加载的bean
 1 value 定义资源名称
 2 blockHandler 定义降级后处理的方法， 默认在同一个类里面，不在需要指定blockHandlerClass
   blockHandler 方法的返回值和参数和原方法一直， 参数后面可以添加一个 BlockException方法。
 */
    @RequestMapping("/CurrenLimiting/{id}")
    @SentinelResource(value = "testCurrentLimiting", blockHandler = "blockHandlerCurreniLmitingFunc" ,blockHandlerClass = CurrenLimitingRules.class)
    public String testCurreniLmiting(@PathVariable(value = "id") String id) {
        System.out.println(id + " " + "CurrenLimiting");
        return id + " " + "CurrenLimiting";
    }

}
