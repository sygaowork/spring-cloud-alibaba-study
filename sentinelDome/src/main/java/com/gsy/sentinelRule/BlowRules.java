package com.gsy.sentinelRule;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRuleManager;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

@Component
public class BlowRules {
    @PostConstruct
    private static void initDegradeRule () {
//       DegradeRule 降级规则  (一般在服务消费方 )
        ArrayList<DegradeRule> degradeRules = new ArrayList<>();
        DegradeRule degradeRule = new DegradeRule();
        degradeRule.setResource("testBlow");
//       设置降级规则异常数  （有慢调用， 异常比例， 异常数）
        degradeRule.setGrade(RuleConstant.DEGRADE_GRADE_EXCEPTION_COUNT);
//        触发熔断的异常数
        degradeRule.setCount(2);
//        触发的最小请求数
        degradeRule.setMinRequestAmount(2);
//        熔断的统计时长 (默认一秒)
        degradeRule.setStatIntervalMs(60*1000);
//        触发降级后 的时间窗口， 在触发后的这个时间里面 都是处于熔断的状态
//        在过了这个的事件后， 会进入 半开放状态， 在半开放状态下 第一次就异常了 那么会直接进入熔断。
        degradeRule.setTimeWindow(10);
        degradeRules.add(degradeRule);
        DegradeRuleManager.loadRules(degradeRules);
    }

     public static String blockHandlerTestBlow (String id, BlockException blockException) {
         System.out.println("降级");
         return "降级";
     }

}
