package com.gsy.sentinelRule;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

@Component
public class CurrenLimitingRules {
    // 加载bean的时候 加载的方法 init-method
    @PostConstruct
    private static void flowRules(){
        //流控规则FlowRule (一般在服务提供方 )
        ArrayList<FlowRule> flowRules = new ArrayList<>();
        FlowRule flowRule = new FlowRule();
        //设置此规则保护的资源名
        flowRule.setResource("testCurrentLimiting");
        //设置流量监控的，qps模式 （有qps模式， 有并发线程数模式）
        flowRule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        //设置limit 1 , 意思是1s 访问多少此 被限流保护
        flowRule.setCount(1);
        flowRules.add(flowRule);
        FlowRule flowRule2 = new FlowRule();
        //设置此规则保护的资源名
        flowRule2.setResource("testException");
        //设置流量监控的，qps模式 （有qps模式， 有并发线程数模式）
        flowRule2.setGrade(RuleConstant.FLOW_GRADE_QPS);
        //设置limit 1 , 意思是1s 访问多少此 被限流保护
        flowRule2.setCount(1);
        flowRules.add(flowRule2);
        FlowRuleManager.loadRules(flowRules);
    }

    public static String blockHandlerCurreniLmitingFunc (String id, BlockException blockException){
        System.out.println("限流");
        return "限流";
    }
}
