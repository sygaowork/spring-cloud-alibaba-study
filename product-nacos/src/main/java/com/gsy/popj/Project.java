package com.gsy.popj;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "project")
@Entity
@Data
public class Project implements Serializable {

    @Id
    public Integer id;
    @Column
    public String name;
    @Column
    public Integer projectNum;

}
