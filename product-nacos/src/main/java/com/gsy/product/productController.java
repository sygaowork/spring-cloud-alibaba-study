package com.gsy.product;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class productController {

    @Value("${server.port}")
    String port;

    @RequestMapping("/{id}")
    public String get(@PathVariable("id") Integer id) throws InterruptedException {
        System.out.println("查询商品id "+ id);
        return "查询商品id "+ id + " " + port;
    }
    @RequestMapping("/getReadTimeout/{id}")
    public String getReadTimeout(@PathVariable("id") Integer id) throws InterruptedException {
        System.out.println("查询商品 getReadTimeout id "+ id);
        Thread.sleep(5000);
        return "查询商品id "+ id + " " + port;
    }

    @RequestMapping("/getException/{id}")
    public String getException(@PathVariable("id") Integer id) throws InterruptedException {
        System.out.println("查询商品 getReadTimeout id "+ id);
        /*
        order-nacos-ribbon-openfeign-nacosconfig-sentinel 整合openfeign和sentinel
        服务提供放报错就会调用消费端 feigin接口实现类的方法进行降级。
         */
        //double res = 1/0;
        /* order-nacos-ribbon-openfeign-nacosconfig-sentinel 整合openfeign和sentinel feign-client-server设置了3s超时所以这个效应有问题
           就会调用消费端 feigin接口实现类的方法进行降级。
         */
        //Thread.sleep(5000);
        return "查询商品id "+ id + " " + port;
    }
}
