package com.gsy.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConfigTest {

    @Value("${spring.cloud.nacos.discovery.username}")
    public static String agrs;

    public String getAgrs(){
        return  agrs;
    }

}
