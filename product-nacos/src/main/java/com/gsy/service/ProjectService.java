package com.gsy.service;

import com.gsy.dao.ProductRes;
import com.gsy.popj.Project;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProjectService {

    @Autowired
    public ProductRes productRes;

    //@Transactional
    @GlobalTransactional
    public String add(String name){
        Project topByName = productRes.findByName(name);
        //List<Project> all = productRes.findAll();
        topByName.setProjectNum(topByName.getProjectNum()+1);
        Project project = productRes.saveAndFlush(topByName);
        return "商品添加";
    }

    @GlobalTransactional
    //@Transactional
    public String decr(String name){
        Project topByName = productRes.findByName(name);
        topByName.setProjectNum(topByName.getProjectNum()-1);
        Project project = productRes.saveAndFlush(topByName);
        //int a = 1/0;
        return "商品减少";
    }


}
