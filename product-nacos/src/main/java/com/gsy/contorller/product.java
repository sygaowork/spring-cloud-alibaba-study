package com.gsy.contorller;

import com.gsy.service.ProjectService;
import com.gsy.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class product {

    @Autowired
    public StockService stockService;

    @Autowired
    public ProjectService projectService;

    @RequestMapping("/add")
    public String add(@RequestParam("name") String name){
        return projectService.add(name);
    }

    @RequestMapping("/decr")
    public String decr(@RequestParam("name") String name){
        return projectService.decr(name);
    }

    @RequestMapping("/stockDecr")
    public String stockDecr() throws InterruptedException {
        System.out.println("下单成功");
        Thread thread = null;
        for (int i = 0; i < 20000; i++) {
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    stockService.dric();
                }
            });
            thread.start();
        }
        thread.join();
        return "hello wrold order-nacos";
    }
}
