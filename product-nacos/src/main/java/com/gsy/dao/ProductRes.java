package com.gsy.dao;

import com.gsy.popj.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRes extends JpaRepository<Project,Integer> {
    Project findByName(String name);
}
