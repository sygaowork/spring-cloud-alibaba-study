package com.gsy.dao;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "stock-service" , path = "/stock")
public interface StoctFeignRes {

    @RequestMapping("/dric")
    public String dric();

}
