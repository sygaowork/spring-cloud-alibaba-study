# Spring-Cloud-Alibaba-Study

#### 介绍
学习SpringCloudAlibaba基础知识记录；

#### 软件架构
后台框架：spring-cloud Hoxton.SR8，springCloud-Alibaba 2.2.5.RELEASE，SpringBoot 2.3.11.RELEASE，nacos-discovery，ribbon，openFeign
sentinel-dashboard-1.8.0 seata-1.3.0

数据库：mysql 5.7.40

代理：nginx 1.14.0

服务器：WSL Ubuntu 18.04.6

环境准备 mysql，nginx，nacos客户端：./environment下提供。也可以在githup上下载，下载时参考spring-cloud-alibaba githup wiki中的版本关系。

nacos：https://github.com/alibaba/nacos/releases

spring-cloud-alibaba：https://github.com/alibaba/spring-cloud-alibaba

nacos 本地单机或本地伪集群环境搭建好之后，修改nacos服务配置的nacos地址为nacos服务地址或者为nginx地址。

### 版本说明
https://github.com/alibaba/spring-cloud-alibaba/wiki/%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E
------------------------------------------
    cloud服务架构组件：
    （1） 服务的注册与发现
    nacos-discovery 提供服务的注册与发现并且提供页面的服务管理平台，nacos默认配置为集群模式，可以内存存储注册信息也可以通过mysql持久信息，
    引入nacos依赖后启动nacos服务后，配置spring-cloud-nacos的服务地址，集群下可以结合nginx反向代理，spirng-cloud-nacos指向nginx地址。
    这样nacos客户端就可以发现和注册对应的服务。
    （2） 服务的配置管理
    nacos-config 提供微服务的配置管理。也可以在nacos的客户端平台上进行修改。
    （3） 服务间的访问调用
    open-fegin 提供服务间的访问调用，可以将服务间的调用接口话，通过启动类当中的@EnableFeginClients开启openfeign，创建接口通过FeginClent
    标记的name和path指向对应的服务。这样服务间的调用就想调用本地接口的形式一样。可以通过配置fegin-client-服务名-config配置服务的响应超时，连接
    超时，拦截器，日志级别。
    （4） 服务的客户端负载均衡
    ribbon 提供客户端的负载均衡，可以通过配置文件 服务名称-ribbon-NFLoadBalancerRuleClassName：指定一个IRule接口的负载均衡的是实现类。
    也可以写一个@Configuration类实现一个IRule返回值得irule方法，在启动类通过@RibbonClient指定服务和配置类。
    ribbon IRule提供的负载均衡方式：
    1. 随机访问 
    2. 轮询访问
    3. 最小并发数访问
    4. 最快响应权重访问
    5. 可用性访问：断路器开开启的服务，访问最大数量限制的服务都是为不可用。
    6. 区域权重访问
    7. 重试策略
    （5） 服务的容错保护机制
    sentinel 提供服务的熔断降级处理和限流操作。@SentinelResource() 需要依赖sentinel-annotation-aspectj, 在启动类配置bean。
    sentienl提供 FlowRule 限流规则和 degradeRule 熔断规则，在sentinel中服务通常被成为资源，通过@SentinelResource 可以设置服务的资源名称
    降级方法和降级方法所在的类。FlowRule一般在服务提供发使用，degradeRule一般在服务消费方使用。 
    FlowRule 限流规则
      阈值类型：qps，访问线程数；
      限流模式：直接，关联（设置关联资源，对一个资源限流，关联资源满足条件后会对该资源限流），链路（设置入口资源：对一个资源去限流，限制某一个入口的限流）。
      流控效果：直接失败，warm up，排队等待。
    degradeRule 熔断规则
      熔断策略：慢调用比例，异常比例，异常数。
    热点规则
      对资源的某个参数进行qps流控。
    系统保护规则
      保护阈值：LOAD, RT, 入口qps, cup使用率, 线程数。
      
    sentinel 也提供页面平台对服务进行检测和资源的规则配置的能力。
    将设置的资源绑定到规则上，DegradeManager.loadRules、FlowRuleManager.loadRules启动规则集合。
    在满足熔断规则熔断后的服务在过了熔断时间后会进入半开放状态，当第一次访问出现异常情况是会直接进入熔断状态；
    
    sentinel 也可以整合OpenFeign继续一起使用，到消费端调用服务端提供的openfeign的接口化服务的时候，服务提供点发生超时，异常等情况不能正常响应。
    降级到接口化服务接口的实现类方法进行降级。需要在@FeignClient的fallback中指定实现类。opneFegin的配置中添加 Fegin-sentinel-enable：true
    （6） seata 分布式事务开源框架 https://github.com/seata/seata
     seata-server db模式需要的表 https://github.com/seata/seata/blob/2.x/script/server/db/mysql.sql  
     seata-server 配置中心默认配置文件  https://github.com/seata/seata/blob/2.x/script/config-center/config.txt
     需要将seata的服务段加入到集群中，需要配置注册中心和配置中心。注册中心支持nacos，erukea 等。如果对数据进行db存储的话，需要使用源码中提供的
     db脚本在客户端和业务数据库建立响应的表比如undolog。在本地服务中引入seata客户端依赖，配置上对应的注册中心和培中心信息。环境正常的话就可以通过
     @GlobalTransactional愉快的解决分布式事务的问题了。
     hmily 分布式事务开源框架 https://github.com/dromara/hmily/tree/master/hmily-demo/hmily-demo-tcc/hmily-demo-tcc-springcloud
     （7） rocketMQ https://rocketmq.apache.org/download/
      
--------------------------------

### zookeeper和eruka的区别
    区别主要在cap理论的支持上，zookeeper在支持的cp模式也就是优先保证节点数据的一致性，eruka支持的是ap优先保证数据的可用性。zookeeper
    中的节点是分为leader和flower节点的。只有leader节点是可以写数据，有leader节点写入数据，再分发给其他节点，当flower接收到写入请求的时候，
    flower节点也会发给leader节点，有leader节点更新在分发给其他节点，在leader节点同步数据的时候和选举leader节点的时候集群都是不可用的。通过这
    样的方式来保证各个节点上同时都有最新数据的副本。eruka各个节点是平等的有限保证集群的可用性，即使有挂掉的节点，也不会影响整个集群的功能。

### 分布式事务
    
    分布式事务一般出现在服务对不同的数据库资源进行操作，或者在服务和服务之间的调用远程调用过程操作数据资源。就产生了分布式事务，
    理论： 有基于2pc就是两阶段提交产生生的XA解决方案。这个方案中表示，需要一个TM 就是TransactionManager，来进行总体的事务管理，每个应用成为ap application Project，ap持有
    不同的资源，具体的事务实例为 rm resource manager，执行的过程中会先进出准备阶段并且将最终的成功或者失败的结果都通知TM，由TM决定各个事务的提交和回滚
    在准备阶段的时候事务不会提交。会持有资源进行等待。
    1 应用层的解决方案。 seata， seata需要在基础环境中添加 seata-server服务，并且配置上seata-server的注册中心参数，配置中心参数，数据存储类型。seata-server支持多种的
    注册中心和配置中心，如果数据类型采用db的方式，可以冲源码的script中获得见表的脚本。和配置中心的默认配置。服务端准备好了之后，本地服务就需要引入seata的依赖并且在业务数据库建立
    undo_log表。在配置中配置好seata-client的注册中心和配置中心信息以及seata-server的地址。然后就可以“愉快”的使用@GlobalTransactional。
    seata在事务rm的阶段是会提交事务的，因为seata会解析语句，如需要回滚的时候进行反向操作。不会一直锁定资源。所以效率上较好。
    2 tcc 轻量级分布式事务解决框架hmily, 需要在数据库为hmily框架创建数据库，在业务数据库在创建如trylog，confirmlog，cancellog的表来处理tcc方式要注意的三个问题，1是幂等性
    就是try，confirm，cancel方法之间要有幂等性不能重复执行，2 空回滚方式在就是说cancel方法要执行，try方法必须是执行过的。3是悬挂问题就是try方法执行的时候，cancel方法必须没有执行
    否则try的操作的资源就没有确认方法可以执行，try操作的资源数据就是所谓的悬挂状态。数据库准备完成后就可以写这3个方法了，hmily狂叫要求try方法需要表示@Hmily注解，并在注解中指定我们的
    confirm方法和concel方法，这3个方法参数必须一致。远程调用的fegin接口中的远程调用方法也需要标记@Hmily注解，根据我们的数据表，在执行每个方法的时候进行判断，首先每个方法都是要做幂等
    判断。判断对应的log表中是否还没有该全局事务id的数据，在try中做幂等和悬挂判断，在cancel中做幂等和空回滚判断。
    3 基于可靠消息的事务处理方案。 以rocketmq消息中间键为例，我们可以通过发送可靠消息，通过其他服消费这个可靠消息来处理分布式事务。rockermq中的 sendMessageInTransaction可以发送
    事务消息来保证本地事务执行后的消息是可靠消息。因为SendMessageInTransaction方法发送消息需要我们提供一个RocketMqLocalTransactionListener的接口，这个接口中需要实现两个方法
    1个是本地事务的执行方法。这个方法是我们执行本地事务并且可以指定消息是需要commit还是rollback。rollback的消息会消息不会被感知和消费。以为SendMessageInTransaction开始发送的是
    半消息。需要在本地事务执行的方法中手动提交。如果在本地事务的执行方法中这个是unknow状态的话，rocketmq会会查我们的另一个接口房啊。本地事务的检查方法。会对我们unknow的半消息重试。
    我们在使用的时候通常也需要一个记录事务执行的表。在这个回查方法中通过查询事务的id是否已经执行过了来进行提交。这样是为了保证如果我本地事务正常执行了，我的消息一定会发出去，而本地事务没有
    成功是不会发送消息的。这是对于生产者来说，对于消费这来说消费之实现的RocketMqMessaageListenerConcurrentlly或者RocketMqMessaageListenerOrderly接口的onMessage方法中也会保证
    消息被消费，如果onmessage发送异常没有正常消费。rockerMq的consumer会进行重试策略去保证消息被消费。通过这种可靠消息的发送和消费来保证事务最终的一致性。
   

