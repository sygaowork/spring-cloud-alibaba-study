package com.gsy.stockService;

import com.gsy.configs.MyOpenFeginConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

//@FeignClient(name = "stock-service", path = "/stock", configuration = MyOpenFeginConfig.class)
@FeignClient(name = "stock-service", path = "/stock")
public interface StockService {
    @RequestMapping("/reduck")
    public String reduck();

    @RequestMapping("/dric/{name}")
    public String dric(@PathVariable("name") String name);

}
