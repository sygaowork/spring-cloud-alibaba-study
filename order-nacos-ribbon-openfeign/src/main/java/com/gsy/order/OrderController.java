package com.gsy.order;

import com.gsy.stockService.StockService;
import com.gsy.productService.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {

    //@Autowired   用了openfeign 所以注释
    //RestTemplate restTemplate;

    @Autowired
    StockService order;

    @Autowired
    ProductService product;

    @Autowired
    StockService stockService;

    @RequestMapping("/add")
    public String add(){
        System.out.println("下单成功");
        //String message = restTemplate.getForObject("http://stock-service/stock/reduck", String.class);
        String message = order.reduck();
        System.out.println("查询产品");
        String s = product.get(2);
        return "hello open feign" + "" + message + " " + s;
    }

    @RequestMapping("/readTimeout/add")
    public String readTimeoutAdd(){
        System.out.println("下单成功");
        //String message = restTemplate.getForObject("http://stock-service/stock/reduck", String.class);
        String message = order.reduck();
        System.out.println("查询产品");
        String s = product.getReadTimeout(3);
        return "hello open feign" + "" + message + " " + s;
    }

    @RequestMapping("/stock")
    public String stock(){
        String a = stockService.dric("食物");
        log.info("食物  调用");
        String b = stockService.dric("药品");
        log.info("药品  调用");
        return "hello open feign " + "" + a + " " + b;
    }
}
