package com.gsy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableFeignClients
@EnableTransactionManagement
//@RibbonClient(name = "stock-service", configuration = MyRibbonConfig.class) //配置类的方式，现在用了配置文件的方式所以注释
public class OrderSeataApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderSeataApplication.class,args);
    }
    //用了openfeign restTemplate 就不需要了

    //@Bean
    //@LoadBalanced
    //public RestTemplate restTemplate (RestTemplateBuilder builder){
    //    RestTemplate restTemplate = builder.build();
    //    return restTemplate;
    //}
}
