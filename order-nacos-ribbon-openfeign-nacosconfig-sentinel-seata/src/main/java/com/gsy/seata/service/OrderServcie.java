package com.gsy.seata.service;

import com.gsy.seata.dao.OrderRes;
import com.gsy.seata.dao.ProjectFeignClientRes;
import com.gsy.seata.popj.Order;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServcie {

    @Autowired
    public OrderRes orderRes;

    @Autowired
    @Lazy
    public ProjectFeignClientRes projectFeignClientRes;

    public List<Order> getAll(){
        List<Order> all = orderRes.findAll();
        return all;
    }
    @GlobalTransactional
    //@Transactional
    public Order xiaDan(Order order) {
        Order save = orderRes.save(order);
        projectFeignClientRes.decr(order.getName());
        int a = 1/0;
        return save;
    }

    public Integer tuiHuo(Order order){
        Integer integer = orderRes.removeById(order.getId());
        projectFeignClientRes.add(order.getName());
        return integer;
    }

}
