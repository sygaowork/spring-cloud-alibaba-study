package com.gsy.seata.dao;

import com.gsy.seata.popj.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRes extends JpaRepository<Order, Integer> {
    Integer removeById(Integer id);
}
