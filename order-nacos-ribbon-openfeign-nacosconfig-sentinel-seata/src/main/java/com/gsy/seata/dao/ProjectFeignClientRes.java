package com.gsy.seata.dao;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "product-service", path = "/product")
public interface ProjectFeignClientRes {

    @RequestMapping("/add")
    public String add(@RequestParam("name") String name);

    @RequestMapping("/decr")
    public String decr(@RequestParam("name") String name);

}
