package com.gsy.seata.contorller;


import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.gsy.seata.popj.Order;
import com.gsy.seata.service.OrderServcie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/seata")
public class Orders {

    @Autowired
    public OrderServcie orderServcie;

    @RequestMapping("/getAll")
    public List<Order> getAll(){
        List<Order> all = orderServcie.getAll();
        return all;
    }

    @RequestMapping("/Buy")
    public Order getBuy(@RequestBody String orderJson){
        JSON parse = JSONUtil.parse(orderJson);
        Order order = parse.toBean(Order.class);
        order = orderServcie.xiaDan(order);
        return order;
    }

}
