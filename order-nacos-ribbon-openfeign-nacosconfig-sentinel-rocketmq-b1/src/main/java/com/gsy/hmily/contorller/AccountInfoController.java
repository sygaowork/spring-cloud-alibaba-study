package com.gsy.hmily.contorller;

import com.gsy.hmily.popj.AccountInfo;
import com.gsy.hmily.service.AccountInfoService;
import com.gsy.hmily.service.RocketMqAccountInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/bank1")
public class AccountInfoController {

    @Autowired
    public AccountInfoService accountInfoService;

    @Autowired
    public RocketMqAccountInfoService rocketMqAccountInfoService;

    @RequestMapping("/getAll")
    public List<AccountInfo> getAll() {
        return accountInfoService.getAll();
    }

    @RequestMapping("/zhuanQian")
    public AccountInfo zhuanQian() {
        return  accountInfoService.zq(2, 2, 10000);
    }

    @RequestMapping("/MQZhuanQian")
    public AccountInfo HmilyZhuanQian() {
        return rocketMqAccountInfoService.sendTranscationMessage( 2,2,10000);
    }

}
