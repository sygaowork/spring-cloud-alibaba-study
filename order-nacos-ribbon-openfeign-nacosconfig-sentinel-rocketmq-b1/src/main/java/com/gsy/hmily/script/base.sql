CREATE TABLE `local_rockmq_log`
(
    `tx_no`       varchar(64) NOT NULL COMMENT '事务id',
    `create_time` datetime DEFAULT NULL,
    PRIMARY KEY (`tx_no`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE `local_rockmq_rollback_log`
(
    `tx_no`       varchar(64) NOT NULL COMMENT '事务id',
    `create_time` datetime DEFAULT NULL,
    PRIMARY KEY (`tx_no`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


-- bank1.account_info definition

CREATE TABLE `account_info`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `name`        varchar(255) DEFAULT NULL COMMENT '户主姓名',
    `card_number` int(11)      DEFAULT NULL COMMENT '银行卡号',
    `password`    varchar(255) DEFAULT NULL COMMENT '帐户密码',
    `balance`     int(11)      DEFAULT NULL COMMENT '帐户余额',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8;