package com.gsy.hmily.popj;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Data
public class AccountInfoTxLog {
    public String Tx;
    public AccountInfo accountInfo;
    public String status;

    public AccountInfoTxLog() {
    }

    public AccountInfoTxLog(String tx, AccountInfo accountInfo, String status) {
        Tx = tx;
        this.accountInfo = accountInfo;
        this.status = status;
    }
}
