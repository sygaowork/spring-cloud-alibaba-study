package com.gsy.hmily.popj;


import lombok.Data;
import net.bytebuddy.dynamic.loading.ClassReloadingStrategy;
import rx.BackpressureOverflow;

import javax.persistence.*;

@Data
@Entity
@Table
public class AccountInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String name;
    public Integer cardNumber;
    public String password;
    public Integer balance;

    public AccountInfo(Integer id, String name, Integer cardNumber, String password, Integer balance) {
        this.id = id;
        this.name = name;
        this.cardNumber = cardNumber;
        this.password = password;
        this.balance = balance;
    }

    public AccountInfo() {
    }
}
