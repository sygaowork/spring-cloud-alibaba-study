package com.gsy.hmily.dao.fegin;

import com.gsy.hmily.popj.AccountInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "order-service-sentinel-rocketmq-bank2" , path = "/bank2")
public interface bankTowRes {

    @RequestMapping("/getAll")
    public List<AccountInfo> getAll();

    @RequestMapping("/shouQian")
    public AccountInfo shouQian(@RequestParam("id") int id, @RequestParam("s")int s);

}
