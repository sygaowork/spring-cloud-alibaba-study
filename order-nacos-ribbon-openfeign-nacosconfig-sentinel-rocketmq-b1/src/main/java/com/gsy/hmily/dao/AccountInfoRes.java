package com.gsy.hmily.dao;

import com.gsy.hmily.popj.AccountInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface AccountInfoRes extends JpaRepository<AccountInfo, Integer> {

    @Query(nativeQuery = true,value = "select count(1) from bank1.local_rockmq_log where tx_no = ?1")
    Integer isTxExists(String tx);

    @Modifying
    @Query(nativeQuery = true, value = "INSERT INTO bank1.local_rockmq_log values (?1, now())")
    Integer addTx(String tx);

    @Query(nativeQuery = true,value = "select count(1) from bank1.local_rockmq_rollback_log where tx_no = ?1")
    Integer isTxRollBackExists(String tx);

    @Modifying
    @Query(nativeQuery = true, value = "INSERT INTO bank1.local_rockmq_rollback_log values (?1, now())")
    Integer addRollBackTx(String tx);

}
