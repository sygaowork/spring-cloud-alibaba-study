package com.gsy.hmily.config;

import org.apache.rocketmq.spring.annotation.ExtRocketMQTemplateConfiguration;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
//自定义RocketMQTemplate  并绑定到@RockerMqTransactionMessage的rocketMQTemplateBeanName属性
@ExtRocketMQTemplateConfiguration(nameServer = "${rocketmq.name-server}", group = "AccountInfoGroup")
public class AccountInfoRocketMqTempLate extends RocketMQTemplate {
}
