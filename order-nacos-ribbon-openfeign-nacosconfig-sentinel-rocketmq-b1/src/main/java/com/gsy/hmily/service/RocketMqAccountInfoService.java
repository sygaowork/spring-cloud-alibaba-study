package com.gsy.hmily.service;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.nacos.common.utils.UuidUtils;
import com.gsy.hmily.config.AccountInfoRocketMqTempLate;
import com.gsy.hmily.dao.AccountInfoRes;
import com.gsy.hmily.dao.fegin.bankTowRes;
import com.gsy.hmily.popj.AccountInfo;
import com.gsy.hmily.popj.AccountInfoTxLog;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Optional;

@Slf4j
@Service
@RocketMQTransactionListener(rocketMQTemplateBeanName = "accountInfoRocketMqTempLate")
public class RocketMqAccountInfoService implements RocketMQLocalTransactionListener {

    @Autowired
    public AccountInfoRes accountInfoRes;

    @Autowired
    public RocketMqAccountInfoService rocketMqAccountInfoService;
    
    @Autowired
    public bankTowRes bankTowRes;

    @Autowired
    public AccountInfoRocketMqTempLate accountInfoRocketMqTempLate;

    @Transactional
    public AccountInfo zq(int zid, int sid, int b){
        log.info("zq ==> ");
        Optional<AccountInfo> byId = accountInfoRes.findById(zid);
        if (byId.isPresent()){
            AccountInfo accountInfo = byId.get();
            accountInfo.setBalance(accountInfo.getBalance()-b);
            AccountInfo save = accountInfoRes.save(accountInfo);
            //AccountInfo accountInfo1 = bankTowRes.shouQian(sid, b); 不用远程调用了等待消息被监听
            log.info("zq: " + save.toString());
            //int a = 1/0;
            return save;
        }

        return null;
    }

    @Transactional
    public AccountInfo sendTranscationMessage (int zid, int sid, int b){
        log.info("sendTranscationMessage ==> ");
        String tx = UuidUtils.generateUuid();
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setId(zid);
        accountInfo.setCardNumber(sid);
        accountInfo.setBalance(b);
        AccountInfoTxLog accountInfoTxLogsucc = new AccountInfoTxLog(tx, accountInfo, "s");
        AccountInfoTxLog accountInfoTxLogerr = new AccountInfoTxLog(tx, accountInfo, "e");
        org.springframework.messaging.Message<AccountInfoTxLog> build1 = MessageBuilder.withPayload(accountInfoTxLogsucc).build();
        org.springframework.messaging.Message<AccountInfoTxLog> build2 = MessageBuilder.withPayload(accountInfoTxLogerr).build();
        TransactionSendResult sendResult1 = accountInfoRocketMqTempLate.sendMessageInTransaction("OneAccountInfo", build1, tx);
        TransactionSendResult sendResult2 = accountInfoRocketMqTempLate.sendMessageInTransaction("OneAccountInfo", build2, tx);
        log.info("sendTranscationMessage ==> " + sendResult1.toString());
        log.info("sendTranscationMessage ==> " + sendResult2.toString());
        log.info("sendTranscationMessage accountInfoRocketMqTempLate ==> " + accountInfoRocketMqTempLate.getProducer().getProducerGroup());
        if (sendResult1.getSendStatus() == SendStatus.SEND_OK && sendResult2.getSendStatus() == SendStatus.SEND_OK){
            return accountInfoRes.findById(zid).get();
        }

        return null;
    }

    @Override
    @Transactional
    public RocketMQLocalTransactionState executeLocalTransaction(Message message, Object tx) {
        log.info("executeLocalTransaction ==> " + (String) tx);
        AccountInfoTxLog accountInfoTxLog = JSONUtil.toBean(JSONUtil.parseObj(message.getPayload()), AccountInfoTxLog.class);
        log.info("executeLocalTransaction  ==> " + accountInfoTxLog);
        try {
            if (accountInfoRes.isTxExists((String) tx) == 0) {
                rocketMqAccountInfoService.zq(
                        accountInfoTxLog.getAccountInfo().getId(),
                        accountInfoTxLog.getAccountInfo().getCardNumber(),
                        accountInfoTxLog.getAccountInfo().getBalance());
                accountInfoRes.addTx((String)tx);
                log.warn("executeLocalTransaction bank1 ==> RocketMQLocalTransactionState.COMMIT");
                accountInfoTxLog.setStatus("s");
            }
            if (accountInfoTxLog.getStatus().equals("s")){
                return RocketMQLocalTransactionState.COMMIT;
            }
            return RocketMQLocalTransactionState.ROLLBACK;
        }catch (Exception e){
            log.warn("executeLocalTransaction catch bank1 ==> RocketMQLocalTransactionState.ROLLBACK");
            if (accountInfoTxLog.getStatus().equals("e")){
                return RocketMQLocalTransactionState.COMMIT;
            }
            return RocketMQLocalTransactionState.ROLLBACK;
        }
    }

    @Override
    @Transactional
    public RocketMQLocalTransactionState checkLocalTransaction(org.springframework.messaging.Message message) {
        AccountInfoTxLog accountInfoTxLog = JSONUtil.toBean(JSONUtil.parseObj(message.getPayload()), AccountInfoTxLog.class);
        log.info("checkLocalTransaction ==> " + accountInfoTxLog.getTx());
        if (accountInfoRes.isTxExists(accountInfoTxLog.getTx()) > 0) {
            log.warn("checkLocalTransaction bank1 ==> RocketMQLocalTransactionState.COMMIT");
            if (accountInfoTxLog.getStatus().equals("s")){
                return RocketMQLocalTransactionState.COMMIT;
            }
            return RocketMQLocalTransactionState.ROLLBACK;
        }
        log.warn("checkLocalTransaction bank1 ==> RocketMQLocalTransactionState.UNKNOWN");
        return RocketMQLocalTransactionState.UNKNOWN;
    }
}
