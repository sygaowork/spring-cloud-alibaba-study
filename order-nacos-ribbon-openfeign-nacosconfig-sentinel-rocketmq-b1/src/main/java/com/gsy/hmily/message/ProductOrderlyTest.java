package com.gsy.hmily.message;

import com.alibaba.nacos.common.utils.UuidUtils;
import com.gsy.hmily.popj.AccountInfo;
import com.gsy.hmily.popj.AccountInfoTxLog;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.selector.SelectMessageQueueByHash;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.List;

@Component
public class ProductOrderlyTest {

    @Autowired
    public RocketMQTemplate rocketMQTemplate;

    public void testSend(){
        AccountInfo accountInfo1 = new AccountInfo(2, "李四用户", 1, "123456", 0);
        AccountInfoTxLog accountInfoTxLog1 = new AccountInfoTxLog("4",accountInfo1,"s");
        Message<AccountInfoTxLog> build1 = MessageBuilder.withPayload(accountInfoTxLog1).build();
        AccountInfo accountInfo2 = new AccountInfo(2, "李四用户", 1, "123456", 0);
        AccountInfoTxLog accountInfoTxLog2 = new AccountInfoTxLog("5",accountInfo1,"s");
        Message<AccountInfoTxLog> build2 = MessageBuilder.withPayload(accountInfoTxLog2).build();
        AccountInfo accountInfo3 = new AccountInfo(2, "李四用户", 1, "123456", 0);
        AccountInfoTxLog accountInfoTxLog3 = new AccountInfoTxLog("6",accountInfo1,"s");
        Message<AccountInfoTxLog> build3 = MessageBuilder.withPayload(accountInfoTxLog3).build();
        rocketMQTemplate.setMessageQueueSelector(new MessageQueueSelector() {
            @Override
            public MessageQueue select(List<MessageQueue> list, org.apache.rocketmq.common.message.Message message, Object o) {
                System.out.println(o);
                int value = o.hashCode() % list.size();
                if (value < 0) {
                    value = Math.abs(value);
                }
                return list.get(value);
            }
        });
        SendResult testTopic1 = rocketMQTemplate.syncSendOrderly("testTopic", build1, build1.getPayload().getTx());
        SendResult testTopic2 = rocketMQTemplate.syncSendOrderly("testTopic", build2, build2.getPayload().getTx());
        SendResult testTopic3 = rocketMQTemplate.syncSendOrderly("testTopic", build3, build3.getPayload().getTx());
        SendResult testTopic4 = rocketMQTemplate.syncSendOrderly("testTopic", build1, build1.getPayload().getTx());
        SendResult testTopic5 = rocketMQTemplate.syncSendOrderly("testTopic", build2, build2.getPayload().getTx());
        SendResult testTopic6 = rocketMQTemplate.syncSendOrderly("testTopic", build3, build3.getPayload().getTx());
        System.out.println(testTopic1);
        System.out.println(testTopic2);
        System.out.println(testTopic3);
        System.out.println(testTopic4);
        System.out.println(testTopic5);
        System.out.println(testTopic6);
    }
}
