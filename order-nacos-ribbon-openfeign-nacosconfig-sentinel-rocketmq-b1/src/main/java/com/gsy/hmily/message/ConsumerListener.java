package com.gsy.hmily.message;

import com.gsy.hmily.dao.AccountInfoRes;
import com.gsy.hmily.popj.AccountInfo;
import com.gsy.hmily.popj.AccountInfoTxLog;
import com.gsy.hmily.service.RocketMqAccountInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@RocketMQMessageListener(consumerGroup = "group_Three", topic = "OneCallBack",
        consumeMode = ConsumeMode.CONCURRENTLY, messageModel = MessageModel.CLUSTERING)
@Component
public class ConsumerListener implements RocketMQListener<AccountInfoTxLog> {

    @Autowired
    public AccountInfoRes accountInfoRes;

    @Autowired
    public RocketMqAccountInfoService rocketMqAccountInfoService;

    @Override
    @Transactional
    public void onMessage(AccountInfoTxLog accountInfoTxLog) {
        log.info("onMessage accountInfo Bank1 start");
        log.info("onMessage executeLocalTransaction Bank1 ==> " + accountInfoTxLog);
        if (accountInfoRes.isTxExists((accountInfoTxLog.getTx())) > 0 &&
                accountInfoRes.isTxRollBackExists(accountInfoTxLog.getTx()) == 0 &&
                accountInfoTxLog.getStatus().equals("e")) {
            log.info("onMessage rollback");
            Optional<AccountInfo> byId = accountInfoRes.findById(accountInfoTxLog.getAccountInfo().getId());
            if (byId.isPresent()) {
                AccountInfo accountInfo = byId.get();
                int balance = accountInfo.getBalance() + accountInfoTxLog.getAccountInfo().getBalance();
                log.info("onMessage  accountInfo Bank2 RollBack " + balance);
                accountInfo.setBalance(balance);
                AccountInfo save = accountInfoRes.save(accountInfo);
                log.info("onMessage  accountInfo Bank2 RollBack end");
            }
            accountInfoRes.addRollBackTx(accountInfoTxLog.getTx());
        }
        if (accountInfoRes.isTxExists((accountInfoTxLog.getTx())) > 0 &&
                accountInfoRes.isTxRollBackExists(accountInfoTxLog.getTx()) > 0 &&
                accountInfoTxLog.getStatus().equals("s")) {
            log.info("onMessage zq");
            rocketMqAccountInfoService.zq(
                    accountInfoTxLog.getAccountInfo().getId(),
                    accountInfoTxLog.getAccountInfo().getCardNumber(),
                    accountInfoTxLog.getAccountInfo().getBalance());
        }
    }
}
