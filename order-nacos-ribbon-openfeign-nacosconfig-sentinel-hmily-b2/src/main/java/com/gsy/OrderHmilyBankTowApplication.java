package com.gsy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableFeignClients
@EnableTransactionManagement
//@RibbonClient(name = "stock-service", configuration = MyRibbonConfig.class) //配置类的方式，现在用了配置文件的方式所以注释
public class OrderHmilyBankTowApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderHmilyBankTowApplication.class,args);
    }
}
