package com.gsy.hmily.popj;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Tcc {
    @Id
    public String txNo;
    public Date createTime;
}
