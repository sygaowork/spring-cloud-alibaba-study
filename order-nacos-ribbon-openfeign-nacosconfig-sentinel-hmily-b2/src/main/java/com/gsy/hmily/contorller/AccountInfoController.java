package com.gsy.hmily.contorller;

import com.gsy.hmily.popj.AccountInfo;
import com.gsy.hmily.service.AccountInfoService;
import com.gsy.hmily.service.HmilyAccountInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/bank2")
public class AccountInfoController {

    @Autowired
    public AccountInfoService accountInfoService;

    @Autowired
    public HmilyAccountInfoService hmilyAccountInfoService;

    @RequestMapping("/getAll")
    public List<AccountInfo> getAll() {
        return accountInfoService.getAll();
    }

    @RequestMapping("/shouQian")
    public AccountInfo shouQian(@RequestParam("id") int id, @RequestParam("s")int s) {
        return  accountInfoService.sq(id, s);
    }

    @RequestMapping("/HmilyshouQian")
    public AccountInfo HmilyshouQian(@RequestParam("id") int id, @RequestParam("s")int s) {
        return  hmilyAccountInfoService.TrySq(id, s);
    }

}
