package com.gsy.hmily.dao;

import com.gsy.hmily.popj.AccountInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountInfoRes extends JpaRepository<AccountInfo, Integer> {

}
