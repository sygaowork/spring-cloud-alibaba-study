package com.gsy.hmily.dao;

import com.gsy.hmily.popj.Tcc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface TccRes extends JpaRepository<Tcc, String> {

    @Query(nativeQuery = true, value ="select count(1) from bank2.local_try_log where tx_no = ?1" )
    Integer isExistTry(String id);
    @Query(nativeQuery = true, value ="select count(1) from bank2.local_confirm_log where tx_no = ?1" )
    Integer isExistConfirm(String id);
    @Query(nativeQuery = true, value ="select count(1) from bank2.local_cancel_log where tx_no = ?1" )
    Integer isExistCancel(String id);

    @Modifying
    @Query(nativeQuery = true, value ="insert into bank2.local_try_log values (?1,now())" )
    Integer addTry(String id);
    @Modifying
    @Query(nativeQuery = true, value ="insert into bank2.local_confirm_log values (?1,now())" )
    Integer addConfirm(String id);
    @Modifying
    @Query(nativeQuery = true, value ="insert into bank2.local_cancel_log values (?1,now())" )
    Integer addCancel(String id);



}
