package com.gsy.hmily.service;

import com.esotericsoftware.minlog.Log;
import com.gsy.hmily.dao.AccountInfoRes;
import com.gsy.hmily.dao.TccRes;
import com.gsy.hmily.popj.AccountInfo;
import lombok.extern.log4j.Log4j2;
import org.dromara.hmily.annotation.Hmily;
import org.dromara.hmily.common.bean.context.HmilyTransactionContext;
import org.dromara.hmily.core.concurrent.threadlocal.HmilyTransactionContextLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Log4j2
@Service
@Transactional
public class HmilyAccountInfoService {


    @Autowired
    public AccountInfoRes accountInfoRes;

    @Autowired
    public TccRes tccRes;

    public List<AccountInfo> getAll() {
        List<AccountInfo> all = accountInfoRes.findAll();
        return all;
    }

    @Hmily(confirmMethod = "ConfirmSq", cancelMethod = "CancelSq")
    public AccountInfo TrySq(int id, int s) {
        String transId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        Log.info("bank2 TrySq ====> " + transId);
        //幂等
        if (tccRes.isExistTry(transId) > 0) return null;
        //悬挂
        if (tccRes.isExistCancel(transId) > 0) return null;
        //本地事务
        //try {
            Optional<AccountInfo> byId = accountInfoRes.findById(id);
            int a = 1 / 0;
            if (byId.isPresent()) {
                AccountInfo accountInfo = byId.get();
                accountInfo.setBalance(accountInfo.getBalance() + s);
                AccountInfo save = accountInfoRes.save(accountInfo);
                Log.info("sq ====> " + save.toString());
                //更新状态
                tccRes.addTry(transId);
                return save;
            }
        //} catch (Exception e) {}
        return null;
    }


    public AccountInfo ConfirmSq(int id, int s) {
        Log.info("bank2 ====> ConfirmSq");
        String transId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        if (tccRes.isExistTry(transId) <= 0) throw new RuntimeException();
        return new AccountInfo();
    }

    public AccountInfo CancelSq(int id, int s) {
        String transId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        Log.info("bank2 CancelSq  ====> " + transId);
        //幂等
        if (tccRes.isExistCancel(transId) > 0) return null;
        //空回归
        if (tccRes.isExistTry(transId) <= 0) return null;
        //本地事务
        Optional<AccountInfo> byId = accountInfoRes.findById(id);
        if (byId.isPresent()) {
            AccountInfo accountInfo = byId.get();
            accountInfo.setBalance(accountInfo.getBalance() - s);
            AccountInfo save = accountInfoRes.save(accountInfo);
            Log.info("sq: " + save.toString());
            //更新状态
            tccRes.addCancel(transId);
            return save;
        }
        return null;
    }

}
