package com.gsy.stock;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stock")
public class StockController {
    @RequestMapping("/reduck")
    public String reduck(){
        System.out.println("扣减库存");
        return "hello world";
    }
}
